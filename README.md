# nested_dict
Extension to traditional Python dictionaries. Allows using lists and tuples as dictionary keys.

### Examples
```
>>> d = NestedDict({'a': 1, 'b': {'c': 3}})
>>> print(d)
{'a': 1, 'b': {'c': 3}}

>>> d['b', 'c']
3

>>> d['b']['c']
3

>>> t = ('b', 'c')
>>> d[t]
3

>>> ('b', 'c') in d
True

>>> for k in d.nestedkeys():
   print(k)
('a',)
('b', 'c')

>>> d.items()
dict_items([('a', 1), ('b', {'c': 3})])

>>> d['b', 'c'] = 5
>>> print(d)
{'a': 1, 'b': {'c': 5}}

>>> d['a', 'd'] = 4
>>> print(d)
{'a': {'d': 4}, 'b': {'c': 5}}

>>> d['a', 'e'] = 4
>>> print(d)
{'a': {'d': 4, 'e': 4}, 'b': {'c': 5}}
```

## Installation

### Cloning the repository
```
git clone https://bitbucket.org/masskat/nested_dict.git
```

### Installing directly from the repository using pip
```
pip install git+https://bitbucket.org/masskat/nested_dict
```

### Upgrading directly from the repository using pip
```
pip install --upgrade git+https://bitbucket.org/masskat/nested_dict
```

### Local installation with pip
Download the archive, unpack it, navigate to the repository directory and run:
```
pip install .
```

### Uninstalling
```
pip uninstall ndict
```

## Development Environment
The library was developed using Python 3.8.
