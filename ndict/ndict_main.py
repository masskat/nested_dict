class NestedDict:
    """
    Class extending functionality of traditional dictionaries. Allows using lists and tuples as keys.

    Examples:

    >>> d = NestedDict({'a': 1, 'b': {'c': 3}})
    >>> print(d)
    {'a': 1, 'b': {'c': 3}}

    >>> d['b', 'c']
    3

    >>> d['b']['c']
    3

    >>> t = ('b', 'c')
    >>> d[t]
    3

    >>> ('b', 'c') in d
    True

    >>> for k in d.nestedkeys():
	   print(k)
    ('a',)
    ('b', 'c')

    >>> d.items()
    dict_items([('a', 1), ('b', {'c': 3})])

    >>> d['b', 'c'] = 5
    >>> print(d)
    {'a': 1, 'b': {'c': 5}}

    >>> d['a', 'd'] = 4
    >>> print(d)
    {'a': {'d': 4}, 'b': {'c': 5}}

    >>> d['a', 'e'] = 4
    >>> print(d)
    {'a': {'d': 4, 'e': 4}, 'b': {'c': 5}}

    """

    def __init__(self, *args, **kwargs):
        """
        Constructor. Uses the same arguments as the dict() method.
        """
        self.dict = dict(*args, **kwargs)


    def __repr__(self):
        """
        Print object.
        """
        return str(self.dict)


    def __iter__(self):
        """
        Used when iteration is needed (ex. in 'for' loop).
        Behaves the same way as standard dictionary.
        """
        return iter(self.dict)


    def __contains__(self, key):
        """
        Check if nested key exists.
        """
        try:
            branch = self[key]
        except (KeyError, TypeError):
            return False
        return True


    def __getitem__(self, keys):
        """
        Provide output based on entered keys.
        """

        # Only single key provided - convert into tuple.
        if not isinstance(keys, tuple):
            keys = (keys,)

        # Go through dictionary.
        branch = self.dict
        for key in keys:
            branch = branch[key]

        # Return leaf value or intermediate dictionary.
        return branch


    def __setitem__(self, keys, value):
        """
        Set value based on provided keys.
        """

        # Only single key provided - convert into tuple.
        if not isinstance(keys, tuple):
            keys = (keys,)

        # Assign value. Referenced within processed dictionary are preserved.
        branch = self.dict
        for key in keys[:-1]:
            if not key in branch or not isinstance(branch[key], dict):
                branch[key] = {}
            branch = branch[key]
        branch[keys[-1]] = value


    def keys(self):
        """
        Wrapping original dictionary keys() method.
        """
        return self.dict.keys()


    def items(self):
        """
        Wrapping original dictionary items() method.
        """
        return self.dict.items()


    def nestedkeys(self):
        """
        Generator method returning tuples of nested keys.
        Equivalent of the original keys() method.
        """

        nested_list = []                # List of nested keys.
        key_list = list(self.keys())    # Initial list of keys.
        do = True                       # While loop control.

        while do:

            # Generate next level keys.
            for key in key_list:
                branch = self[key] if isinstance(key, str) else self[tuple(key)]
                if isinstance(branch, dict):
                    for new_key in branch:
                        if isinstance(key, str):
                            key_list.append([key] + [new_key])
                        else:
                            key_list.append(key + [new_key])
                    key_list.remove(key)

            # Continue if there are still branches that didn't reach leaf values.
            do = False
            for key in key_list:
                branch = self[key] if isinstance(key, str) else self[tuple(key)]
                if isinstance(branch, dict):
                    do = True

        # Return one nested key tuple at a time.
        for key in key_list:
            if isinstance(key, str):
                yield (key,)
            else:
                yield tuple(key)


    def nesteditems(self):
        """
        Equivalent of the original items() method.
        Generator method returning (<nested_key>, value) tuples.
        """
        for nested_key in self.nestedkeys():
            yield (nested_key, self[nested_key])



class NestedIncDict(NestedDict):
    """
    Special version of the NestedDict class used to build sophisticated dictionaries for counting purposes.
    Instead of 'KeyError' exception returns 0 when specific branch doesn't exist. This way it is not necessary to initialize specific branch (e.g. to increment the leaf value).
    All other methods are inherited from the NestedDict class.
    """

    def __getitem__(self, keys):
        """
        Provide output based on entered keys. Return 0 if a value for entered key does not exist.
        """
        try:
            branch = NestedDict.__getitem__(self, keys)
        except KeyError:
            branch = 0
        return branch
