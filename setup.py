import setuptools
from setuptools import setup

setup(
    name="ndict",
    version="1.0.2",
    author="masskat",
    author_email="masskat@example.com",
    description="Nested Dictionary",
    long_description="Nested Dictionary library. Extends functionality of traditional dictionaries. Allows using lists and tuples as keys.",
    long_description_content_type="text",
    url="https://bitbucket.org/masskat/nested_dict.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3',
)
